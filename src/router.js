import Vue from 'vue'
import Router from 'vue-router'
import AddTemplate from '@/components/templates/AddTemplate'
import Login from '@/components/login/Login'
import Logs from '@/components/logs/Logs'
import Users from '@/components/users/Users'
import Reports from '@/components/reports/Reports'
import Alarms from '@/components/alarms/Alarms'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      name: 'login',
      path: '/login',
      component: Login
    },
    {
      name: 'logs',
      path: '/logs',
      component: Logs
    },
    {
      name: 'alarms',
      path: '/alarms',
      component: Alarms
    },
    {
      name: 'reports',
      path: '/reports',
      component: Reports
    },
    {
      name: 'users',
      path: '/users',
      component: Users
    },
    {
      path: '/templates/add',
      name: 'add-template',
      component: AddTemplate
    }
  ]
})
