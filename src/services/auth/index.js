import axios from 'axios'
import { assign } from 'lodash-es'

import router from '@/router'
import store from '@/store'
import { STATUS_CODES, COMMON_PAGES } from '@/constants'

const ENDPOINTS = {
  LOGIN: '/auth/login',
  LOGOUT: '/auth/logout',
  REGISTER: '/auth/register',
  LOGGED_IN_USER: '/auth/me'
}

export default class AuthService {
  constructor () {
    this.setAuthHeader()
    this.setLanguageHeader()
    this.setResponseInterceptor()
  }

  /**
   * Sets auth data in localStorage in order to remember the users token
   * @param {String} data
   */
  setLocalStorageAuthData (token) {
    localStorage.setItem('token', token)
  }

  /**
   * Sets users data in localStorage
   * @param {Object} user
   */
  setLocalStorageUserData (user) {
    localStorage.setItem('user', JSON.stringify(user))
  }

  /**
   * Handles auth change
   * @param {String} token
   */
  async handleAuthChange (token) {
    try {
      this.setLocalStorageAuthData(token)
      this.setAuthHeader()
      const response = await this.getLoggedInUser()
      this.setLocalStorageUserData(response)
    } catch (error) {
      throw error
    }
  }

  /**
   * Clear localStorage from auth data
   */
  clearLocalStorageAuthData () {
    localStorage.removeItem('token')
    localStorage.removeItem('user_id')
    localStorage.removeItem('user')
  }

  /**
   * Checks if user is logged in by checking localStorage
   *
   * @returns {Boolean}
   */
  isLoggedIn () {
    return !!localStorage.getItem('token')
  }

  /**
   * Sets Authorization header in order to make authenticated requests
   */
  setAuthHeader () {
    this.setHeader('Authorization', `Bearer ${localStorage.getItem('token')}`)
  }

  /**
   * Removes axios default authorization header
   */
  removeAuthHeader () {
    delete axios.defaults.headers['Authorization']
  }

  /**
   * Gets the currently logged in user
   *
   * @returns {Promise}
   */
  async getLoggedInUser () {
    try {
      const response = await axios.post(ENDPOINTS.LOGGED_IN_USER)
      return response.data
    } catch (error) {
      throw error
    }
  }

  /**
   * Attempt login
   * @param {Object} data
   */
  async attemptLogin (data) {
    try {
      const response = await axios.post(ENDPOINTS.LOGIN, data)
      return response
    } catch (error) {
      throw error
    }
  }

  /**
   * Log out currently logged in user
   */
  async logout () {
    try {
      this.clearLocalStorageAuthData()
      store.dispatch('clearAll')
      this.removeAuthHeader()
    } catch (error) {
      throw error
    }
  }

  /**
   * Registers the user
   * @param {Object} data
   * @param {String} token
   *
   * @returns {Promise}
   */
  async register (data) {
    try {
      const response = await axios.post(ENDPOINTS.REGISTER, data)
      return response
    } catch (error) {
      throw error
    }
  }

  /**
   * Set a default axios header
   * @param {String} header
   * @param {String} value
   */
  setHeader (header, value) {
    assign(axios.defaults.headers, {
      [header]: value
    })
  }

  setResponseInterceptor () {
    axios.interceptors.response.use(
      response => {
        return response
      },
      error => {
        if (error.response) {
          const { status } = error.response

          switch (status) {
            case STATUS_CODES.FORBIDDEN:
              router.replace(COMMON_PAGES.FORBIDDEN)
              return Promise.reject(error)
            case STATUS_CODES.NOT_FOUND:
              router.replace(COMMON_PAGES.NOT_FOUND)
              return Promise.reject(error)
            case STATUS_CODES.UNAUTHORIZED:
              store.dispatch('account/logout')
              return Promise.reject(error)
            default:
              return Promise.reject(error)
          }
        }

        return Promise.reject(error)
      }
    )
  }
}

export const authService = new AuthService()
