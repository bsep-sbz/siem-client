import axios from 'axios'
import { BASE_API_URL } from '@/constants'

const ENDPOINTS = {
  LOGS_PER_MACHINE_REPORTS: '/reports/logs-per-machine',
  LOGS_PER_SYSTEM_REPORTS: '/reports/logs-per-system',
  ALARMS_PER_MACHINE_REPORTS: '/reports/alarms-per-machine',
  ALARMS_PER_SYSTEM_REPORTS: '/reports/alarms-per-system'
}

const getTimeLimitQueryParams = (timeLimit) => {
  return `?start_time=${timeLimit.start}&end_time=${timeLimit.end}`
}

export default class ReportsService {
  constructor () {
    axios.defaults.baseURL = BASE_API_URL
  }

  getLogsPerMachineReports (timeLimit) {
    return axios.get(`${ENDPOINTS.LOGS_PER_MACHINE_REPORTS}${getTimeLimitQueryParams(timeLimit)}`)
  }

  getLogsPerSystemReports (timeLimit) {
    return axios.get(`${ENDPOINTS.LOGS_PER_SYSTEM_REPORTS}${getTimeLimitQueryParams(timeLimit)}`)
  }

  getAlarmsPerMachineReports (timeLimit) {
    return axios.get(`${ENDPOINTS.ALARMS_PER_MACHINE_REPORTS}${getTimeLimitQueryParams(timeLimit)}`)
  }

  getAlarmsPerSystemReports (timeLimit) {
    return axios.get(`${ENDPOINTS.ALARMS_PER_SYSTEM_REPORTS}${getTimeLimitQueryParams(timeLimit)}`)
  }
}

export const reportsService = new ReportsService()
