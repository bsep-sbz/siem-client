import axios from 'axios'
import { BASE_API_URL } from '@/constants'

const ENDPOINTS = {
  TEMPLATES: '/config/templates'
}

export default class TemplatesService {
  constructor () {
    axios.defaults.baseURL = BASE_API_URL
  }

  create (data) {
    return axios.post(this.getTemplatesUrl(), data)
  }

  getTemplatesUrl () {
    return ENDPOINTS.TEMPLATES
  }
}

export const templatesService = new TemplatesService()
