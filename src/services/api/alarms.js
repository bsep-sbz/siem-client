import axios from 'axios'
import { BASE_API_URL } from '@/constants'

const ENDPOINTS = {
  ALARMS: '/alarms'
}

export default class AlarmsService {
  constructor () {
    axios.defaults.baseURL = BASE_API_URL
  }

  index () {
    return axios.get(this.getAlarmsUrl())
  }

  getAlarmsUrl () {
    return ENDPOINTS.ALARMS
  }
}

export const alarmsService = new AlarmsService()
