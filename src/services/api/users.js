import axios from 'axios'
import { BASE_API_URL } from '@/constants'

const ENDPOINTS = {
  USERS: '/users',
  USER: '/users/:userId'
}

export default class UsersService {
  constructor () {
    axios.defaults.baseURL = BASE_API_URL
  }

  index () {
    return axios.get(this.getUsersUrl())
  }

  update (item) {
    return axios.put(this.getUserUrl(item.id), { riskFactor: item.riskFactor })
  }

  getUserUrl (id) {
    return ENDPOINTS.USER.replace(':userId', id)
  }

  getUsersUrl () {
    return ENDPOINTS.USERS
  }
}

export const usersService = new UsersService()
