import axios from 'axios'
import { BASE_API_URL } from '@/constants'

const ENDPOINTS = {
  LOGS: '/logs'
}

export default class LogsService {
  constructor () {
    axios.defaults.baseURL = BASE_API_URL
  }

  index () {
    return axios.get(this.getLogsUrl())
  }

  getLogsUrl () {
    return ENDPOINTS.LOGS
  }
}

export const logsService = new LogsService()
