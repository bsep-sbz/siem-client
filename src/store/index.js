import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  appLoading: false
}

const mutations = {
  SET_APP_LOADING (state, payload) {
    state.appLoading = payload
  }
}

const actions = {}

const getters = {
  appLoading: state => state.appLoading
}

const store = new Vuex.Store({
  state,
  mutations,
  actions,
  getters,
  modules: {

  }
})

export default store
