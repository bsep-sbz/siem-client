import {
  API_ROUTE,
  API,
  APP,
  APP_URL,
  API_URL
} from '@/constants'

let config = {
  getEnvironments () {
    const hostname = this.transformHostname(window.location.hostname)

    return {
      localhost: 'dev',
      [hostname !== 'localhost' ? hostname : APP_URL]: 'stage'
    }
  },

  getBackendHostnames () {
    const hostname = this.transformHostname(window.location.hostname)

    return {
      dev: 'http://localhost',
      stage: `${window.location.protocol}//${
        hostname !== 'localhost' ? hostname : API_URL
      }`
    }
  },

  getApiUrl () {
    return this.getHostName() + API_ROUTE
  },

  getHostName () {
    return this.getBackendHostnames()[this.getEnv()]
  },

  getEnv () {
    const hostname = this.transformHostname(window.location.hostname)

    return this.getEnvironments()[hostname]
  },

  isDevEnv () {
    return this.getEnv() === 'dev'
  },

  isStageEnv () {
    return this.getEnv() === 'stage'
  },

  isProductionEnv () {
    return this.getEnv() === 'production'
  },

  transformHostname (hostname) {
    return hostname.replace(APP, API)
  }
}

export { config }
