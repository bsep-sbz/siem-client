const STATUS_CODES = {
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  VALIDATION_FAILED: 422,
  INTERNAL_SERVER_ERROR: 500
}

const BASE_API_URL = 'http://localhost:8000'

const LOGICAL_OPERATORS = [
  'and',
  'or'
]

const ACTION_TYPES = [
  'new',
  'set',
  'insert',
  'get',
  'delete'
]

const ACTION_ENTITIES = [
  'Alarm'
]

const COMPARISON_OPERATORS = [
  'gt',
  'ge',
  'lt',
  'le',
  'eq',
  'neq',
  'regex'
]

const ACCUMULATION_FUNCTION_TYPES = [
  'count',
  'min',
  'max',
  'sum',
  'avg'
]

const ACCUMULATION_CONSTRAINT_FIELDS = [
  'intValue',
  'doubleValue',
  'floatValue'
]

const CONSTRAINT_FIELDS = [
  'size'
]

const FRAME_TYPES = [
  'time',
  'length'
]

const FACT_ENTITY_TYPE_LOG = 'Log'

const FACT_ENTITY_TYPES = [
  FACT_ENTITY_TYPE_LOG
]

const FACT_ENTITY_TYPE_FIELDS = {
  [FACT_ENTITY_TYPE_LOG]: [
    'timestamp',
    'ip',
    'message'
  ]
}

const USER_RISKS = [
  'low',
  'moderate',
  'high',
  'extreme'
]

export {
  STATUS_CODES,
  LOGICAL_OPERATORS,
  FACT_ENTITY_TYPES,
  FACT_ENTITY_TYPE_FIELDS,
  COMPARISON_OPERATORS,
  FRAME_TYPES,
  BASE_API_URL,
  ACTION_TYPES,
  ACTION_ENTITIES,
  CONSTRAINT_FIELDS,
  ACCUMULATION_FUNCTION_TYPES,
  ACCUMULATION_CONSTRAINT_FIELDS,
  USER_RISKS
}
